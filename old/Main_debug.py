# import sys
import time
import os
from Image_Visual_Features import ImageVisualFeatures
import cv2
from pickle_functions import load_var, save_var
import sonar_process, score_functions
from read_json import ScanData
import re
import csv
import numpy as np
import matplotlib.pyplot as plt
from score_functions import rmse
import matplotlib.animation as anim

num_of_DM_per_rows = 10
num_of_column = 5
generate_map = False
for_one = True
gen_dm = True

dir = '/home/shaul/Dropbox/MultiModalSLAM/Data/alone_5'
single_val = 5
#TODO: Normalize by signal transmit

def main(experiment_location):
    total_time_start = time.time()
    # ######################  get raw data  ##########################
    if generate_map:
        print "Start generate map ..."
        jsonfiles = []
        for path, subdirs, files in os.walk(experiment_location):
            for fileNames in files:
                if fileNames.endswith(".json"):
                    jsonfiles.append(os.path.join(path, fileNames))
        # ###################### create DataMatrix ######################
        if len(jsonfiles) == 0:
            print "Error - No Json files"
        else:
            print "No. of Json file: %s" % len(jsonfiles)
            map = []
            for paths in jsonfiles:
                start_time = time.time()
                map.append(DataMatrix(ScanData(paths), experiment_location))
                print "Finish %s DataMatrixes" % len(map)
                print "time: " + str(time.time() - start_time)
            save_var(map, 'map_5_dec.pickle')
    else:
        print "Loading Map from memory ..."
        map = load_var('map_5_dec.pickle')
        print "Map on memory"
    init=1
    if for_one:
            vec_sigmas = np.arange(0.05, 0.5, 0.05)
            vec_weight = np.arange(0.05, 1, 0.05)
            nndist_vec = np.arange(0.5, 1, 0.05)
            fo = os.open('/home/shaul/Dropbox/params_2.txt', os.O_RDWR | os.O_CREAT)
            fd = os.fdopen(fo, 'w+')
            fd.write("Start \n")
            fd.write("**************************************************\n")
            single_val = 18
            single_DM = '/scan%s.json'%single_val
            if gen_dm:
                # print "Start Check DM"
                checkPoint = DataMatrix(ScanData(dir+single_DM), dir)
                save_var(checkPoint, 'DM.pickle')
                # print "End Check DM"
            else:
                checkPoint = load_var('DM.pickle')
            scores_vision = score_functions.calc_1_vs_all_scores_by_feature(map, checkPoint, 0.1, 0.1, 0.1, 0.1, 'vision')
            for a in nndist_vec:
                for b in vec_weight:
                    for c in vec_sigmas:
                        for d in vec_sigmas:
                            scores_sonar = score_functions.calc_1_vs_all_scores_by_feature(map, checkPoint, a, b, c, d, 'sonar')
                            value = rmse(np.asarray(scores_vision),np.asarray(scores_sonar))
                            fd.write("\t nndist: " + str(a) + "\t fft_w: " + str(b) +  "\t sigma_fft: " + str(c) +"\t sigma_distance: " + str(d) + "\n")
                            fd.write("\t value:" + str(value)+"\n")
            fd.close()

            fo = os.open('/home/shaul/Dropbox/params_1.txt', os.O_RDWR | os.O_CREAT)
            fd = os.fdopen(fo, 'w+')
            fd.write("Start \n")
            fd.write("**************************************************\n")
            single_val = 12
            single_DM = '/scan%s.json'%single_val
            if gen_dm:
                # print "Start Check DM"
                checkPoint = DataMatrix(ScanData(dir+single_DM), dir)
                save_var(checkPoint, 'DM.pickle')
                # print "End Check DM"
            else:
                checkPoint = load_var('DM.pickle')
            scores_vision = score_functions.calc_1_vs_all_scores_by_feature(map, checkPoint, 0.1, 0.1, 0.1, 0.1, 'vision')
            for a in nndist_vec:
                for b in vec_weight:
                    for c in vec_sigmas:
                        for d in vec_sigmas:
                            scores_sonar = score_functions.calc_1_vs_all_scores_by_feature(map, checkPoint, a, b, c, d, 'sonar')
                            value = rmse(np.asarray(scores_vision),np.asarray(scores_sonar))
                            fd.write("\t nndist: " + str(a) + "\t fft_w: " + str(b) +  "\t sigma_fft: " + str(c) +"\t sigma_distance: " + str(d) + "\n")
                            fd.write("\t value:" + str(value)+"\n")
            fd.close()


            # scores = score_functions.fusion(scores_vision, scores_sonar)
            # scores_only_sonar = score_functions.fusion_For_sonar(scores_vision, scores_sonar)
            # scores_only_vision = score_functions.fusion_For_vision(scores_vision, scores_sonar)
            # # print "End Fusions scores"
            # pltHeatMaps(scores, scores_only_sonar, scores_only_vision, init)

    else:
        for json_num in range(1, 25):
            checkPoint = DataMatrix(ScanData(dir + '/scan%s.json' % json_num), dir)
            scores_vision, scores_sonar = score_functions.calc_1_vs_all_scores_by_feature(map, checkPoint, a, b, c, d, True)
            scores = score_functions.fusion(scores_vision, scores_sonar)
            scores_only_sonar = score_functions.fusion_For_sonar(scores_vision, scores_sonar)
            scores_only_vision = score_functions.fusion_For_vision(scores_vision, scores_sonar)
            pltHeatMaps(scores, scores_only_sonar, scores_only_vision, init)
            print "End %s Plot" % json_num
            init = 0
    print "Total time: %s" % str(time.time() - total_time_start)


class DataMatrix:
    def __init__(self, scan, experiment_location):
        self.odom_x = scan.odom_x
        self.odom_y = scan.odom_y
        self.phi = scan.phi
        self.meta = scan.meta
        self.scan_number = re.findall('scan(.*?).json', self.meta)
        self.scan_descriptors = self.get_scan_descriptors(scan, experiment_location)

    @staticmethod
    def get_scan_descriptors(scan, experiment_location):
        temp_scan_descriptor = {}
        for i in range(0, len(scan.capture)):
            temp_scan_descriptor[scan.capture[i].angle] = CaptureDescriptors(scan.capture[i], experiment_location)
        return temp_scan_descriptor


class CaptureDescriptors:
    def __init__(self, angled_meta, experiment_location):
        self.vision = self.get_features_from_rgb_image(experiment_location + angled_meta.image)
        self.sonar = self.get_features_from_sonar_rec(os.path.join(experiment_location, angled_meta.sonar[1:]))
        self.temp = None
        self.meta = [angled_meta.sonar[1:], angled_meta.image]

    @staticmethod
    def get_features_from_sonar_rec(sonar_path):
        # start_time = time.time()
        signal = sonar_process.SonarSignal(sonar_path)
        # print (" ***get features from Sonar: " + sonar_path + ' time: ' + str(time.time() - start_time) + "***\n")
        return signal

    @staticmethod
    def get_features_from_rgb_image(rgb_image_path):
        # start_time = time.time()
        bgr_image = cv2.imread(rgb_image_path)
        vf = ImageVisualFeatures("SIFT", bgr_image)
        # print (" ***get features from image: " + rgb_image_path + ' time: ' + str(time.time() - start_time) + "***\n")
        return vf


def export_2_csv(cross_dist, csv_name, save_im=False):
    with open(csv_name, 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for c_d in cross_dist:
            spamwriter.writerow(c_d)
    if save_im:
        try:
            cross_dist_im = np.array(cross_dist) * 255
            dot = csv_name.find(".")
            im_name = csv_name[:dot] + ".jpg"
            cv2.imwrite(im_name, cross_dist_im)
        except Exception as e:
            im_name = csv_name[:dot] + ".jpg"
            print ("error in " + im_name)
            print (e.message)


def pltHeatMaps(combined, only_sonar, only_vision, flag):
    plt.subplot(1, 3, 1)
    data = np.asarray(combined)
    test = arrData(data, num_of_DM_per_rows, num_of_column)
    plt.pcolor(test.transpose(), cmap='Blues')
    plt.clim(0, 1)
    if flag == 1 :
        plt.colorbar()
    plt.title('Combined')

    plt.subplot(1, 3, 2)
    data = np.asarray(only_sonar)
    test = arrData(data, num_of_DM_per_rows, num_of_column)
    plt.pcolor(test.transpose(), cmap='Blues')
    plt.clim(0, 1)
    if flag == 1 :
        plt.colorbar()
    plt.title('Sonar')

    plt.subplot(1, 3, 3)
    data = np.asarray(only_vision)
    test = arrData(data, num_of_DM_per_rows, num_of_column)
    plt.pcolor(test.transpose(), cmap='Blues')
    plt.clim(0, 1)
    if flag == 1 :
        plt.colorbar()
    plt.title('vision')
    if for_one:
        plt.show(block=True)
    else:
        plt.show(block=False)
    plt.pause(0.001)


def arrData(data, num_of_DM_per_rows, num_of_column):
    data = data[:]
    for multiplier in range(1, num_of_column, 2):
        backward_data = data[num_of_DM_per_rows * multiplier : num_of_DM_per_rows * multiplier + 10]
        backward_data = backward_data[::-1]
        data[num_of_DM_per_rows * multiplier : num_of_DM_per_rows * multiplier + 10] = backward_data
        test = data.reshape(num_of_column, num_of_DM_per_rows)
        return test


if __name__ == '__main__':
    main(dir)























