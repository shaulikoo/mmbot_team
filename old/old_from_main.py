__author__ = 'Shaul'



# need_2_save_features = False
#
#
#
#
# if exp_data is None:
#
# if need_2_save_exp_data:
#
#
# # ~~~~~~~~~~~~~~~~~~~~~~get features~~~~~~~~~~~~~~~~~~~~~~
# print (" ***start extract features...***")
# scan_descriptors_single_image, scan_descriptors_image_pairs = get_scans_features(exp_data)
# 	if need_2_save_features:
# 		save_var([scan_descriptors_single_image, scan_descriptors_image_pairs], 'features.pickle')
# 	print (" ***start extract sonar features...***")
# 	sonar_scan_descriptors_single_image, sonar_scan_descriptors_image_pairs = get_sonar_features(exp_data)
# 	# ~~~~~~~~~~~~~~~~~~~~~~get distances~~~~~~~~~~~~~~~~~~~~~~
#
#
#
# def get_scans_features(exp_data):
#     start_feature_time = time.time()
#     read_vars = load_var('features.pickle')
#     if read_vars is not None:
#         scan_descriptors_single_image, scan_descriptors_image_pairs = read_vars
#         print ("*******************************************************************************")
#         print ("***load features from disk: " + str(time.time() - start_feature_time) + "*** ")
#         print ("*******************************************************************************\n")
#     else:
#         scan_descriptors_single_image = []
#         scan_descriptors_image_pairs = []
#         scan_ind = 0
#
#         # fast calcs
#         # exp_data.scans_data = exp_data.scans_data[:1]
#         for scan in exp_data.scans_data:
#             start_scan_time = time.time()
#
#             scan_single_image = Scan_Descriptor.ScanDescriptor(exp_data.image_folder_path, scan,
#                                                                Scan_Descriptor.ScanDescriptor.SCENE_SINGLE_IMAGE)
#             scan_image_pair = Scan_Descriptor.ScanDescriptor(exp_data.image_folder_path, scan,
#                                                              Scan_Descriptor.ScanDescriptor.SCENE_IMAGE_PAIR)
#             scan_descriptors_single_image.append(scan_single_image)
#             scan_descriptors_image_pairs.append(scan_image_pair)
#
#             done_scan_time = time.time()
#
#             print ("~~~scan_" + str(scan_ind) + " out of " + str(len(exp_data.scans_data)) + " done~~~" )
#             print ("***scan_" + str(scan_ind) + " run time: " + str(done_scan_time - start_scan_time) + "***\n")
#             scan_ind += 1
#         print ("*******************************************************************************")
#         print ("***total feature calc time: " + str(time.time() - start_feature_time) + "*** ")
#         print ("*******************************************************************************\n")
#     return scan_descriptors_single_image, scan_descriptors_image_pairs
#
#
# def get_sonar_features(exp_data):
#     start_feature_time = time.time()
#     read_vars = load_var('features.pickle')
#     if read_vars is not None:
#         scan_descriptors_single_image, scan_descriptors_image_pairs = read_vars
#         print ("*******************************************************************************")
#         print ("***load features from disk: " + str(time.time() - start_feature_time) + "*** ")
#         print ("*******************************************************************************\n")
#     else:
#         sonar_scan_descriptors_single_image = []
#         sonar_scan_descriptors_image_pairs = []
#         scan_ind = 0
#
#         # fast calcs
#         # exp_data.scans_data = exp_data.scans_data[:1]
#         for scan in exp_data.scans_data:
#             start_scan_time = time.time()
#
#             scan_single_image = Scan_Descriptor.ScanDescriptor(exp_data.image_folder_path, scan,
#                                                                Scan_Descriptor.ScanDescriptor.SCENE_SINGLE_IMAGE,
#                                                                ["SIFT", "SIFT_SONAR"])
#             scan_image_pair = Scan_Descriptor.ScanDescriptor(exp_data.image_folder_path, scan,
#                                                              Scan_Descriptor.ScanDescriptor.SCENE_IMAGE_PAIR,
#                                                              ["SIFT", "SIFT_SONAR"])
#             sonar_scan_descriptors_single_image.append(scan_single_image)
#             sonar_scan_descriptors_image_pairs.append(scan_image_pair)
#
#             done_scan_time = time.time()
#
#             print ("~~~scan_" + str(scan_ind) + " out of " + str(len(exp_data.scans_data)) + " done~~~" )
#             print ("***scan_" + str(scan_ind) + " run time: " + str(done_scan_time - start_scan_time) + "***\n")
#             scan_ind += 1
#         print ("*******************************************************************************")
#         print ("***total feature calc time: " + str(time.time() - start_feature_time) + "*** ")
#         print ("*******************************************************************************\n")
#     return sonar_scan_descriptors_single_image, sonar_scan_descriptors_image_pairs
#
#
# def get_cross_distances(scan_features, is_only_sonar=False):
#     cross_dist = []
#     # cross distances
#     scan_ind = 0
#     start_dist_calc_time = time.time()
#     dist_by_theta = {}
#     detailed_dist = []
#     for curr_scan_instance in scan_features:
#         scan_detailed_dist = []
#         start_scan_time = time.time()
#         dist_instance = []
#         # compare_ind = 0
#         for scan_2_compare in scan_features:
#             # total_distance, dist_by_theta = curr_scan_instance.get_scan_distance_theta_vs_theta(scan_2_compare)
#             if not is_only_sonar:
#                 total_distance, dist_by_theta = curr_scan_instance.get_scan_distance_all_thetas(scan_2_compare)
#             else:
#                 total_distance, dist_by_theta = curr_scan_instance.get_scan_distance_all_thetas(scan_2_compare,
#                                                                                                 is_only_sonar)
#             dist_instance.append(total_distance)
#             scan_detailed_dist.append(dist_by_theta)
#         cross_dist.append(dist_instance)
#         detailed_dist.append(scan_detailed_dist)
#         done_scan_time = time.time()
#         print ("~~~calc cross distances for scan" + str(scan_ind) + " out of " + str(len(scan_features)) + " done~~~")
#         print ("***scan_" + str(scan_ind) + " run time: " + str(done_scan_time - start_scan_time) + "***\n")
#         scan_ind += 1
#     print ("******************************************************************************")
#     print ("***total dist calc time: " + str(time.time() - start_dist_calc_time) + "*** ")
#     print ("******************************************************************************\n")
#     return cross_dist, detailed_dist
#     pass
#
#
# def export_2_csv(cross_dist, csv_name, save_im=False):
#     with open(csv_name, 'wb') as csvfile:
#         spamwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
#         for c_d in cross_dist:
#             spamwriter.writerow(c_d)
#     if save_im:
#         try:
#             cross_dist_im = np.array(cross_dist) * 255
#             dot = csv_name.find(".")
#             im_name = csv_name[:dot] + ".jpg"
#             cv2.imwrite(im_name, cross_dist_im)
#         except Exception as e:
#             print ("error in " + im_name)
#             print (e.message)
