__author__ = 'Shaul'


    # features_check = []
    # #Create Panoramic ref
    # for angle in twister:
    #     ######### Put all sonar features into list
    #     for fet in DM_ref.scan_descriptors[angle].sonar.features:
    #         features_ref.append(fet)
    #     for fet in DM_check.scan_descriptors[angle].sonar.features:
    #         features_check.append(fet)
    # # #Check each angle for value
    # score = matcher(features_ref, features_check, nndist=nndist_val, fft_weight=fft_weight_val, distance_weight=1-fft_weight_val, sigma_fft=sigma_fft_val, sigma_distance=sigma_distance_val)
    # return score
    #Create  Panoramic ref

    # scan_descriptors_ref = DM_ref.scan_descriptors
    # scan_descriptors_check = DM_check.scan_descriptors
    # first = True
    # for angle in twister:
    #     data_ref = scan_descriptors_ref[angle].sonar.features
    #     data_check = scan_descriptors_check[angle].sonar.features
    #     line_lone = 2*13
    #     if first:
    #         panoramic_matrix_ref = np.array(data_ref.reshape(1, line_lone))
    #         panoramic_matrix_check = np.array(data_check.reshape(1, line_lone))
    #         first = False
    #     else:
    #         data_ref = scan_descriptors_ref[angle].sonar.features
    #         panoramic_matrix_ref = np.concatenate((panoramic_matrix_ref, data_ref.reshape(1, line_lone)), axis=0)
    #         panoramic_matrix_check = np.concatenate((panoramic_matrix_check, data_check.reshape(1, line_lone)), axis=0)
    #
    # score = matcher_mfcc(panoramic_matrix_ref, panoramic_matrix_check, nndist=0.85)
    # return score


def second_smallest(numbers):
    first, second = max(numbers), max(numbers)
    for n in numbers:
        if n < first:
            first, second = n, first
        elif first < n < second:
            second = n
    return second


def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())




def matcher(descriptors1, descriptors2, nndist=0.75, fft_weight=0.5, distance_weight=0.5,sigma_fft=0.3,sigma_distance =0.2):
    fit = numpy.zeros((len(descriptors1), len(descriptors2)))
    row = 0
    for feature1 in descriptors1:
        colomn = 0
        for feature2 in descriptors2:
            # fft_dis = np.corrcoef(feature1.fft, feature2.fft)[0][1]
            # fft_score = np.exp(-0.5*((fft_dis-1)/sigma_fft)**2)
            fft_dis = rmse(feature1.fft, feature2.fft)
            fft_score = np.exp(-0.5*((fft_dis)/sigma_fft)**2)
            distance_score = np.exp(-0.5*((feature2.distance-feature1.distance)/sigma_distance)**2)
            fit[row, colomn] = fft_weight * (1-fft_score) + distance_weight * (1-distance_score)
            colomn += 1
        row += 1
    good_matches = []
    for m in range(len(descriptors1)):
        if min(fit[m, :]) < nndist * second_smallest(fit[m, :]):
            good_matches.append(min(fit[m, :]))
    if not good_matches:
        return 0
    elif min(good_matches) == 0.0 and max(good_matches) == 0.0:
        return 1
    else:
        return 1 - np.asarray(good_matches).mean()

